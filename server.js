var path=require('path');
var express=require('express');
var mongoose=require('mongoose');
var bodyParser=require('body-parser');
var passport=require('passport');
var authController=require('./controllers/auth');
var argv=require('optimist').argv;

//Express Application COnfiguration
var app=express();
app.use(bodyParser.urlencoded({
	extended:true
}));

app.use(passport.initialize());



var port=process.env.PORT||3000;
var router=express.Router();

//database configuration
mongoose.connect('mongodb://root:root@'+argv.dbip+':27017/admin');

//Index
router.get('/',function(req,res){
	res.json({message:'You are running dangerously low on BEER!'});

});

//About
router.get('/about/',function(req,res){
	res.json({message:'This is just a tutorial for NODE API'});
});





// app.use('/api/t',rTest); //Test
app.use('/api',router);

/*
	AUTO-LOADER for Routes
	======================
*/
var normalPath=path.join(__dirname,"routes");
require("fs").readdirSync(normalPath).forEach(function(file){
	app.use('/api/'+path.basename(file,".js"),require('./routes/'+file));
	//console.log(path.basename(file,".js")+",");
});

//app.use('/api/beers',require('./routes/beer'));


app.listen(argv.port,argv.ip);
console.log('Insert beer on port '+argv.port);
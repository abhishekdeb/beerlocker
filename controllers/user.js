var User=require('../model/user');

module.exports={
	create:function(req,res){
	
		var user=new User();
		
		//Catch data && Validation
		user.username=req.body.usrname;
		user.password=req.body.pwd;

		//Save Data
		user.save(function(err){
			if(err)
				res.send(err);

			res.json({message:'User added to Administration!',data:user});
		});

	},
	readAll:function(req,res){
		User.find(function(err,users){
			if(err)
				res.send(err);

			res.json(users);
		});
	},
	read:function(req,res){
		User.findById(req.params.user_id,function(err,user){
			if(err)
				res.send(err);

			res.json(user);
		});
	},
	update:function(req,res){
		User.findById(req.params.user_id,function(err,user){
			if(err)
				res.send(err);

			//catch data
			user.username=req.body.usrname;
			user.password=req.body.pwd;

			user.save(function(err){
				if(err)
					res.send(err);

				res.json(user);
			});
		});
	},
	delete:function(req,res){
		User.findByIdAndRemove(req.params.user_id,function(err,user){
			if(err)
				res.send(err);

			res.json({message:'User Removed from Administration!'});
		});
	}
};
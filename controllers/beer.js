var Beer=require('../model/beer');

module.exports={
	create:function(req,res){
	
		var beer=new Beer();
		
		//Catch data && Validation
		beer.name=req.body.name;
		beer.type=req.body.type;
		beer.quantity=req.body.quantity;
		beer.userId=req.user._id;

		//Save Data
		beer.save(function(err){
			if(err)
				res.send(err);

			res.json({message:'Beer added to locker!',data:beer});
		});

	},
	readAll:function(req,res){
		Beer.find({userId:req.user._id},function(err,beers){
			if(err)
				res.send(err);

			res.json(beers);
			res.set("Connection", "close");
		});
	},
	read:function(req,res){
		Beer.find({userId:req.user._id,_id:req.params.beer_id},function(err,beer){
			if(err)
				res.send(err);

			res.json(beer);
		});
	},
	update:function(req,res){
		Beer.update({userId:req.user._id,_id:req.params.beer_id},{quantity:req.body.quantity},function(err,num,beer){
			if(err)
				res.send(err);

			beer.save(function(err){
				if(err)
					res.send(err);

				res.json({message:num + ' updated.'});
			});
		});
	},
	delete:function(req,res){
		Beer.remove({userId:req.user._id,_id:req.params.beer_id},function(err){
			if(err)
				res.send(err);

			res.json({message:'Beer Removed from Locker!'});
		});
	}
};
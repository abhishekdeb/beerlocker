var express=require('express');
var router=express.Router();

var authController=require('../controllers/auth');
var beerController=require('../controllers/beer');


//Functional Routes
router.route('/')
	//CREATE
	.post(authController.isAuthenticated,beerController.create)
	//READ ALL
	.get(authController.isAuthenticated,beerController.readAll);	


//Functional Route for SIngular Use
router.route('/:beer_id')
	//READ
	.get(authController.isAuthenticated,beerController.read)
	//UPDATE
	.put(authController.isAuthenticated,beerController.update)
	//DELETE
	.delete(authController.isAuthenticated,beerController.delete);

module.exports=router;
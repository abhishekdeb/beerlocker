var express=require('express');
var router=express.Router();

var authController=require('../controllers/auth');
var Beer=require('../model/beer');

router.route('/')
	.get(authController.isAuthenticated, function(req,res){
		//res.send('Now Test Running OK!');
		Beer.find({userId:req.user._id},function(err,data){
			if(err) res.send(err);

			res.json(data);
		});
	});

module.exports=router;
var express=require('express');
var router=express.Router();

var authController=require('../controllers/auth');
var userController=require('../controllers/user');

//Functional Routes
router.route('/')
	//CREATE
	.post(userController.create)
	//READ ALL
	.get(authController.isAuthenticated,userController.readAll);	

//Functional Route for SIngular Use
router.route('/:user_id')
	//READ
	.get(userController.read)
	//UPDATE
	.put(userController.update)
	//DELETE
	.delete(userController.delete);

module.exports=router;